import java.util.*;
public class Main1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int team[] = new int[n];

		for (int i = 0; i < n; i++) {
			try {
				String s = input.next();
				int num = Integer.parseInt(s);
				team[i] = num;
			} catch (NumberFormatException e) {
				System.out.println(e.toString());
				i--;
			}
		}
		String arrString = Arrays.toString(team);
		System.out.print(arrString); // 按格式输出[number1,number2,number3,****]
	}

}
	
