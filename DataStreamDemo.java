import java.io.*;
public class DataStreamDemo{


	public static void main(String[] args) throws IOException
	{
		//创建数据输出流对象
		DataOutputStream out=new DataOutputStream(new FileOutputStream("netfee.txt"));
		//记录网费的数组
		double[] netfees={0.30,0.24,0.42,0.87,1.50,0.96,0.69};
		//记录上网时间的数组
		int[] minutes={10,8,13,29,50,32,23};
		String[] items={"星期日","星期一","星期二","星期三","星期四","星期五","星期六"};
		
		//写入数据
		for (int i=0;i<netfees.length ;i++ )
		{
			out.writeInt(minutes[i]);
			out.writeChar('\t');               //Tab
			out.writeDouble(netfees[i]);
			out.writeChar('\n');
		}
		out.close();

		//创建数据输入流对象
		DataInputStream in=new DataInputStream(new FileInputStream("netfee.txt"));
		double netfee;
		int minute;
		double total=0.0;
		
		try
		{
			System.out.println("你的上网费是:");
			int i=0;
			while (true)
			{
				minute=in.readInt();
				in.readChar();           //忽略Tab
				netfee=in.readDouble();
				in.readChar();
				System.out.println(items[i]+"\t"+minute+"分钟"+"\t小计：￥"+netfee);
				total=total+netfee;
				i++;
			}
		}
		catch (EOFException e)
		{
		}
		System.out.println("总费用：￥"+total);
		in.close();
	}
}