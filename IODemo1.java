import java.io.*;
public class IODemo1{
	public static void main(String[] args)
	{
		int i;
		int b;
		try
		{
			FileInputStream in=new FileInputStream("source.txt");
			i=0;
			while((b=in.read())!=-1)
			{
				System.out.println((char)b);
				i++;
			}
			System.out.println();
			System.out.println("TOTAL="+i);
			in.close();
		}
		catch (Exception e)
		{
			System.out.println("No file!!!");
		}
	}
}